﻿namespace XO.Game.AI.Analizer
{
    internal static class TerminatedStateChecker
    {
        public static bool IsTerminated(string[,] state)
        {
            // Use WinnerChecker to determine if the state is terminated
            return WinnerChecker.CheckForWin(state, "x") || WinnerChecker.CheckForWin(state, "o");
        }
    }
}
