﻿namespace XO.Game.AI.Analizer
{
    internal static class PlayerIdentifier
    {
        public static string IdentifyPlayer(string[,] state)
        {
            int countX = 0;
            int countO = 0;

            // Count the number of 'x' and 'o' in the board
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (state[i, j].ToLower() == "x")
                    {
                        countX++;
                    }
                    else if (state[i, j].ToLower() == "o")
                    {
                        countO++;
                    }
                }
            }

            // Determine who should play next
            return countX > countO ? "o" : "x";
        }
    }
}
