﻿namespace XO.Game.AI.Analizer
{
    internal static class TerminatedStateUtility
    {
        public static int ToNumerical(string[,] state)
        {
            bool xWins = WinnerChecker.CheckForWin(state, "x");
            bool oWins = WinnerChecker.CheckForWin(state, "o");

            if (xWins)
            {
                return 1; // 'x' wins
            }
            if (oWins)
            {
                return -1; // 'o' wins
            }
            return 0; // No one wins
        }

        public static bool IsDraw(string[,] state)
        {
            foreach (string cell in state)
            {
                if (cell == " ")
                {
                    return false; // If any cell is empty, it's not a draw
                }
            }
            return true; // No empty cells, it's a draw
        }
    }
}
