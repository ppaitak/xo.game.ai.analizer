﻿namespace XO.Game.AI.Analizer
{
    internal static class StateActionsProvider
    {
        public static string[][,] ProvideStateActions(string[,] state)
        {
            string nextPlayerSymbol = PlayerIdentifier.IdentifyPlayer(state);

            List<string[,]> possibleActions = new List<string[,]>();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (state[i, j] == " ")
                    {
                        // Create an action with only the next move
                        string[,] action = EmptyStateCreator.CreateEmptyState();
                        action[i, j] = nextPlayerSymbol;
                        possibleActions.Add(action);
                    }
                }
            }

            return possibleActions.ToArray();
        }
    }
}
