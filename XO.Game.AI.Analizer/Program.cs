﻿using XO.Game.AI.Analizer;

while (true)
{
    string[,] currentState = new string[3, 3];

    try
    {
        currentState = StateReader.ReadState();
        StateValidator.ValidateState(currentState);
    }
    catch (ArgumentException ex)
    {
        Console.WriteLine(ex.Message);
        Console.WriteLine("Please try again!\n");
        continue;
    }

    StateWriter.WriteState(currentState);

    string nextPlayer = PlayerIdentifier.IdentifyPlayer(currentState);
    Console.WriteLine($"\nNext player to move: {nextPlayer}.\n");

    int bestScore;
    switch (nextPlayer)
    {
        case "x":
            bestScore = int.MinValue;
            break;
        case "o":
            bestScore = int.MaxValue;
            break;
        default:
            throw new NotImplementedException();
    }

    string[,] bestMove = EmptyStateCreator.CreateEmptyState();

    foreach (var action in StateActionsProvider.ProvideStateActions(currentState))
    {
        string[,] newState = StateResultProvider.ProvideStateResult(currentState, action);

        int currentScore;
        switch (nextPlayer)
        {
            case "x":
                currentScore = MinMaxAI.Maximize(newState);
                break;
            case "o":
                currentScore = MinMaxAI.Minimize(newState);
                break;
            default:
                throw new NotImplementedException();
        }

        if (nextPlayer == "x" && currentScore > bestScore)
        {
            bestScore = currentScore;
            bestMove = action;
        }
        else if (nextPlayer == "o" && currentScore < bestScore)
        {
            bestScore = currentScore;
            bestMove = action;
        }
    }

    string[,] updatedState = StateResultProvider.ProvideStateResult(currentState, bestMove);

    // Output or return the updated state
    Console.WriteLine("Best move for player " + nextPlayer + ":");
    StateWriter.WriteState(updatedState);

    // Ask the user if they want to continue
    Console.WriteLine("\nDo you want to enter another state? (yes/no)\n");

    string answer = Console.ReadLine()!.ToLower();
    if (answer != "yes")
    {
        break;
    }
}