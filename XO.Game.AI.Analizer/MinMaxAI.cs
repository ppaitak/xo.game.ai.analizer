﻿namespace XO.Game.AI.Analizer
{
    internal static class MinMaxAI
    {
        public static int Minimize(string[,] state)
        {
            int score = TerminatedStateUtility.ToNumerical(state);

            if (score == 1 || score == -1 || TerminatedStateUtility.IsDraw(state))
            {
                return score;
            }

            int bestScore = int.MaxValue;

            foreach (var action in StateActionsProvider.ProvideStateActions(state))
            {
                string[,] newState = StateResultProvider.ProvideStateResult(state, action);
                int value = Maximize(newState);
                bestScore = Math.Min(bestScore, value);
            }

            return bestScore;
        }

        public static int Maximize(string[,] state)
        {
            int score = TerminatedStateUtility.ToNumerical(state);

            if (score == 1 || score == -1 || TerminatedStateUtility.IsDraw(state))
            {
                return score;
            }

            int bestScore = int.MinValue;

            foreach (var action in StateActionsProvider.ProvideStateActions(state))
            {
                string[,] newState = StateResultProvider.ProvideStateResult(state, action);
                int value = Minimize(newState);
                bestScore = Math.Max(bestScore, value);
            }

            return bestScore;
        }
    }
}
