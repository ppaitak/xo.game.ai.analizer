﻿namespace XO.Game.AI.Analizer
{
    internal static class StateWriter
    {
        public static void WriteState(string[,] state)
        {
            Console.WriteLine("\nTic-Tac-Toe State:\n");

            for (int i = 0; i < 3; i++)
            {
                if (i > 0)
                {
                    Console.WriteLine("---|---|---");
                }

                for (int j = 0; j < 3; j++)
                {
                    if (j > 0)
                    {
                        Console.Write("|");
                    }
                    Console.Write($" {state[i, j]} ");
                }
                Console.WriteLine();
            }
        }
    }
}
