﻿namespace XO.Game.AI.Analizer
{
    internal static class StateValidator
    {
        public static void ValidateState(string[,] currentState)
        {
            int countX = 0;
            int countO = 0;

            // Count the number of 'x' and 'o' in the board
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (currentState[i, j].ToLower() == "x")
                    {
                        countX++;
                    }
                    else if (currentState[i, j].ToLower() == "o")
                    {
                        countO++;
                    }
                }
            }

            // Check for various invalid state conditions and throw exceptions with specific messages
            if (countX > countO + 1)
            {
                throw new ArgumentException("Invalid state: too many 'x's.");
            }
            if (countO > countX)
            {
                throw new ArgumentException("Invalid state: too many 'o's.");
            }

            int xWins = WinnerChecker.CountWins(currentState, "x");
            int oWins = WinnerChecker.CountWins(currentState, "o");

            if (xWins > 0 && oWins > 0)
            {
                throw new ArgumentException("Invalid state: both players cannot win simultaneously.");
            }

            if (xWins > 0 && countO >= countX)
            {
                throw new ArgumentException("Invalid state: 'o' cannot move after 'x' wins.");
            }

            if (oWins > 0 && countX > countO)
            {
                throw new ArgumentException("Invalid state: 'x' cannot move after 'o' wins.");
            }
        }
    }
}
