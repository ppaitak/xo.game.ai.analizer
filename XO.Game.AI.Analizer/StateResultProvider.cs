﻿namespace XO.Game.AI.Analizer
{
    internal static class StateResultProvider
    {
        public static string[,] ProvideStateResult(string[,] state, string[,] actionState)
        {
            string[,] newState = (string[,])state.Clone();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    // Apply the move from the actionState and break out of the loops
                    if (actionState[i, j] != " ")
                    {
                        newState[i, j] = actionState[i, j];

                        // Since actionState has only one move, break the inner and outer loops
                        return newState;
                    }
                }
            }

            return newState;
        }
    }
}
