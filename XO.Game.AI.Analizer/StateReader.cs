﻿namespace XO.Game.AI.Analizer
{
    internal static class StateReader
    {
        public static string[,] ReadState()
        {
            string[,] currentState = new string[3, 3];

            Console.WriteLine("Enter each row of the Tic-Tac-Toe state as a separate line.");
            Console.WriteLine("Each line should contain 3 characters (x or o or space, e.g., \"xox\" or \"o  \"):\n");

            for (int i = 0; i < 3; i++)
            {
                string inputRow = Console.ReadLine()!;

                if (inputRow.Length != 3 || !IsValidRow(inputRow))
                {
                    throw new ArgumentException("Invalid input. Each row must contain exactly 3 characters ('x', 'o', ' ').");
                }

                for (int j = 0; j < 3; j++)
                {
                    currentState[i, j] = inputRow[j].ToString();
                }
            }

            return currentState;
        }

        private static bool IsValidRow(string row)
        {
            foreach (char c in row.ToLower())
            {
                if (c != 'x' && c != 'o' && c != ' ')
                {
                    return false;
                }
            }
            return true;
        }
    }
}
