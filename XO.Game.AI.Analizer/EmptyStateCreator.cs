﻿namespace XO.Game.AI.Analizer
{
    internal static class EmptyStateCreator
    {
        public static string[,] CreateEmptyState()
        {
            string[,] action = new string[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    action[i, j] = " ";
                }
            }
            return action;
        }
    }
}
