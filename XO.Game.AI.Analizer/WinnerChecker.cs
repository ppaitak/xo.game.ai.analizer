﻿namespace XO.Game.AI.Analizer
{
    internal static class WinnerChecker
    {
        public static bool CheckForWin(string[,] board, string player)
        {
            return CountWins(board, player) > 0;
        }

        public static int CountWins(string[,] board, string player)
        {
            int winCount = 0;

            // Check rows, columns, and diagonals for wins
            for (int i = 0; i < 3; i++)
            {
                if (IsWinningCombination(board[i, 0], board[i, 1], board[i, 2], player)) winCount++;
                if (IsWinningCombination(board[0, i], board[1, i], board[2, i], player)) winCount++;
            }

            if (IsWinningCombination(board[0, 0], board[1, 1], board[2, 2], player)) winCount++;
            if (IsWinningCombination(board[0, 2], board[1, 1], board[2, 0], player)) winCount++;

            return winCount;
        }

        private static bool IsWinningCombination(string cell1, string cell2, string cell3, string player)
        {
            return cell1 == player && cell2 == player && cell3 == player;
        }
    }
}
